
# Solutions

### Task 1

```sh
$ docker run -d --name web1 nginx:1.9.11
$ docker run --rm -it --link web1 centos:7.2.1511 
[root@c36cf9096ed8 /]#  curl http://web
```

```sh
$ cat /etc/hosts
```

### Task 2

```sh
$ docker network create academy-net
```

```sh
$ ip address
...
4: br-11076e1de06b: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN 
    link/ether 02:42:04:1e:5a:e3 brd ff:ff:ff:ff:ff:ff
    inet 172.19.0.1/16 scope global br-11076e1de06b
       valid_lft forever preferred_lft forever
...
```
Docker creates an interface named `br-*` for each network.

```sh
$ docker run -d --net academy-net --name web nginx:1.9.11
$ docker run --rm -it --net academy-net centos:7.2.1511 
[root@c36cf9096ef9 /]# curl http://web
[root@c36cf9096ef9 /]# cat /etc/hosts
```

```sh
$ docker network inspect academy-net
```

Since version 1.10 Docker networking does not add the web container 
IP address into the /etc/hosts file. Instead it provides a real DNS server 
which is able to resolve the container names to its IP addresses.

### Task 3

```sh
docker run -d --net academy-net \
    --name academy-welcome-service \
    registry:5000/academy-welcome-service:1.0
```

### Task 4

```sh
$ docker run -d --net academy-net \
    -e POSTGRES_DB=academy \
    -e POSTGRES_USER=academy \
    -e POSTGRES_PASSWORD=academy \
    --name=academy-db \
    registry:5000/academy-db:1.0

$ docker run -d --net academy-net \
    -p 8080:8080 \
    -e DB_HOST=academy-db \
    -e WELCOME_SVC_URL=http://academy-welcome-service:8080/welcome \
    --name academy-frontend \
    trt/academy-frontend:1.0
```
