# Volume lab

## Task 1
Goals is to experience the possible problems with permissions.

* create (as user root) a folder on your docker host /root/demofolder
* check the permission of the folder
* mount the folder on a busybox container on /tmp and touch a file under /tmp *Note use --rm -it /bin/sh to run the busybox container*
* check the permission and the user you are running
* mount the folder on jenkins:1.625.3 container /tmp and touch a file

## Task 2
Goal is a postgres db container where the data is decoupled

### Task 2.1
Create a data volume container

* create a data volume container **academy-db-data** 
    * the data volume container shall be based on postgres image
    * the data volume container shall have to volumes
        * /var/lib/postgresql/data
        * /tmp
    * copy from the volume-lab/sqlscripts/init.sql into the /tmp dir on the data volume container
    * the data volume container shall be named academy-data *Note: If you rerun the postgres server start command, 
  delete the data volume container as well, so the server will initialize from scratch*

### Task 2.2
Start the academy-db container

* the academy-db container shall have a database called academy, a user academy and the password academy
* the server shall be named academy-db
* mount academy-db-data as volume container on academy-db
* login to the running container and init the schema with the following command
```
 psql -d academy -U academy -a -f /tmp/init.sql
```
* to verify, just do the following 
```
  psql -d academy -U academy -a
  academy=# select * from message;
```

### Task 2.3 (Optional)
Another option to run psql command would be via unix socket. Goal is to make academy-db socket available on
another container.

* stop and delete academy-db server detached
* run a new instance of the academy-db server
    * mount as before the volume data container
    * add a new volume on /var/run/postgresql/
    * the container shall be removed automatically    

```
<your docker run command> sh -c 'psql -d academy -U academy -a'
```


### Task 3
Let's now create a backup of the data while our academy-db is still running.

* connect again to to academy-db
* the data backup data should be on data volume container we passed earlier to academy-db
* use busybox to zip and moving the backup to the filesystem of the docker host 

---

> In reality for postgres db init, you would copy the script to /docker-entrypoint-initdb.d (Dockerfile).

---


