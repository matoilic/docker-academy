
# Weave Scope

This is a web based [tool](https://www.weave.works/products/weave-scope/) to visualize your docker containers.
  
Run `scope launch` and access the web page http://10.0.0.100:4040/. 
