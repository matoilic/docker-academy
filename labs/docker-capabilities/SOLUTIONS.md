
# Solutions

## Task 1

By default the time cannot be set inside a docker container.

```sh
docker run -it --rm busybox:1.24.1
/ # date -s 09:41
date: can't set date: Operation not permitted
/ # date
Wed Mar  9 21:10:17 UTC 2016
```

By adding the SYS_TIME capability we gain the permission to set the time.

```sh
docker run -it --rm --cap-add SYS_TIME busybox:1.24.1
/ # date -s 09:41
Wed Mar  9 09:41:00 UTC 2016
```